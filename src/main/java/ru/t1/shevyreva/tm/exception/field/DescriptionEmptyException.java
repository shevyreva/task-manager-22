package ru.t1.shevyreva.tm.exception.field;

public final class DescriptionEmptyException extends AbsrtactFieldException {

    public DescriptionEmptyException() {
        super("Error! Description is empty!");
    }

}
