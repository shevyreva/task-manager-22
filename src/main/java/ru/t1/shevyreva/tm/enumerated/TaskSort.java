package ru.t1.shevyreva.tm.enumerated;

import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.NameComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name.", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status.", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created.", DateComparator.INSTANCE::compare);

    private final String displayName;

    private final Comparator<Task> comparator;

    TaskSort(final String displayName, final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
