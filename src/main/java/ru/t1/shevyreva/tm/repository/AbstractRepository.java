package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.IRepository;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    public M add(final M model) {
        records.add(model);
        return model;
    }

    public List<M> findAll() {
        return records;
    }

    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    public M findOneById(final String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    public void removeAll() {
        records.clear();
    }

    public M removeOne(final M model) {
        records.remove(model);
        return model;
    }

    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
        return model;
    }

    public void removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        removeOne(model);
    }

    public boolean existsById(final String id) {
        final M model = findOneById(id);
        return model != null;
    }

    public Integer getSize() {
        return records.size();
    }

}
