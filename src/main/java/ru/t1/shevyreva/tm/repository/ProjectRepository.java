package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

}
