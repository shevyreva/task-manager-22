package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    public void removeAll(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    public void removeAll(final List<M> models) {
        for (M model : models)
            this.removeOne(model);
    }

    public M removeOne(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }

    public M removeOne(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return this.removeOne(model);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        this.removeOne(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        return model != null;
    }

    @Override
    public Integer getSize(final String userId) {
        return (int) records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}
