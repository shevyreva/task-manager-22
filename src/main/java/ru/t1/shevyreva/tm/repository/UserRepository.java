package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean isLoginExist(final String login) {
        return records.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExist(final String email) {
        return records.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

    @Override
    public User findByLogin(final String login) {
        return records.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

}
