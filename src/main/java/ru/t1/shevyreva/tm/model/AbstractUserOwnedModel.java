package ru.t1.shevyreva.tm.model;

public class AbstractUserOwnedModel extends AbstractModel {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
