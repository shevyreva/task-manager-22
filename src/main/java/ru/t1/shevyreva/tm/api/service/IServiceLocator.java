package ru.t1.shevyreva.tm.api.service;

public interface IServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
