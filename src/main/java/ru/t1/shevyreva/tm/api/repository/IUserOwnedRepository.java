package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    void removeAll(String userId);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M removeOne(String userId, M model);

    M removeOneById(String userId, String id);

    void removeOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    Integer getSize(String userId);

}
