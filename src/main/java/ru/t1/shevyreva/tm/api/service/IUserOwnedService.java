package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends IService<M> {

    M add(String userId, M model);

    void removeAll(String userId);

    M removeOne(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    Integer getSize(String userId);

    M removeOneById(String userId, String id);

    void removeOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

}
