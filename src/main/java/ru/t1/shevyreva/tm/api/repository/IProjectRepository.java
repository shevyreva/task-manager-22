package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
