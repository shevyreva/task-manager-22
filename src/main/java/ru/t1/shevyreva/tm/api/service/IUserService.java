package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User removeOne(User model);

    User updateUser(String id, String firstName, String middleName, String lastName);

    User setPassword(String id, String password);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
