package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserUnlockedCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Unlock user.";

    private final String NAME = "user-unlock";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("Enter user login:");
        final String login = TerminalUtil.nextLine();
        getUserService().findByLogin(login).setLocked(false);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
