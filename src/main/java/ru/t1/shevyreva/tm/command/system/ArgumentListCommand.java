package ru.t1.shevyreva.tm.command.system;

import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Show argument list.";

    private final String NAME = "arguments";

    private final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
