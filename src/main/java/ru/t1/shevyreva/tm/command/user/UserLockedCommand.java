package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserLockedCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Lock user.";

    private final String NAME = "user-lock";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("Enter user login:");
        final String login = TerminalUtil.nextLine();
        getUserService().findByLogin(login).setLocked(true);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
